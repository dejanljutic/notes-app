# Notes app

Notes app is a simple javascript application for taking quick notes in your browser. They can be edited and removed. On the homepage they can be sorted alphabetically, by time created, and by time last edited.

## UUID

A third party library for generating an unique ID property for every note. 
It is imported to index.html as a local .js file, because the link provided in the course is not working properly. It is meant to be a read-only library, thus the formating is non-existent.

## Launching

The app needs to be launched using live-server. 

To install live-server you first need to install [Node](https://nodejs.org/en/).

By installing Node, you also install another program called npm - Node Package Manager. To see if it is correctly installed, run:
```bash
npm -v
```

Now to install live-server you need to run the following command:
```bash 
npm install -g live-server
```

Finally, to host the application you need to cd into the folder in which the app folder is located, and run the following command (Notes-app being the folder name):
```bash
live-server Notes-app
```

## Future improvements

CSS will be added, and a free hosting service will be used, making it much easier to use the app.